
# Challenge Breakdown by Zone #

## Exile ##
#### Arkship
None
#### Northern Wilds
Rootbrute Killing Challenge - Complete
Skreech Killing Zone - Complete
Xenobite Egg Destruction - Complete

#### Algoroc

* Grinding Up Gronyx - Supported [Two Test Runs. 1 Gold, 1 High Silver]
* Cartel Slayer - Supported.  Full completion each  time
* Protector Rejector - Supported. Gold both times
* Grim Reaper - Supported. 1 Silver, 1 Gold
* Skug Egg Destroyer - Supported. 2 silvers.  changed position in profile and should be better now.
* SwiftPaw Splayer - Two Bronzes. Changed Position.  I had it way after the swiftpaw mission so the challenge ran during normal questing rather than dedicated grinding on the challenge. Changed this and will report back.
* Tremor Ridge Defender - Supported. Complete both times.
* Smish Shrooms - Silver Both Times
* Cnamid Killer - Gold Both Times
* Roan Tipping - ***Unsupported*** - Cannot tell when it's night time :/
* The Grid- ***Unsupported*** - Parkour jump challenge
* Minefile Mayhem - Supported, completed both times.
* Scale the loftite Pillar - ***Unsupported*** Parkour
* Energized Stones - Supported. Silver both times
* Collect Crowe Masks - Supported Silver both times
* Scrap Yard - Supported. Gold both times
* Loftite Collector - ***Unsupported*** Mesh can't get us there :/ requires fancy jumps.

#### Thyad

* One Exile's Trash - One run, silver (was soooo damned close)
* Diary Detritus Deliverance - Unsupported. bad mesh issues in Thyad.  Can't get up there easily.

